
import axios from 'axios'
import { useState } from 'react'
const projectID = '30f4b2e0-e3d6-48bf-bef0-40d82c84dddf';

const LoginForm = () => {

    const [username, setusername] = useState('')
    const [password, setpassword] = useState('')
    const [error, seterror] = useState('');




    const handlechange = (e) => {
        setusername(e.target.value)
    }
    const handleSubmit = async (e) => {
        e.preventDefault();

        const authObject = { 'Project-ID': projectID, 'User-Name': username, 'User-Secret': password };

        try {

            await axios.get('https://api.chatengine.io/chats', { headers: authObject });

            localStorage.setItem('username', username)
            localStorage.setItem('password', password)
            window.location.reload();
            seterror('');
        } catch (error) {
            seterror('ooops, incorrect username or password ')

        }
    };


    return (

        <div className="wrapper">
            <div className="form">

                <h1 className="title"> chat Appication </h1>
                <form onSubmit={handleSubmit}>

                    <input
                        type="text"
                        value={username}
                        onChange={handlechange}
                        className="input"
                        placeholder="Username" required />

                    <input
                        type="password"
                        value={password}
                        onChange={(e) => setpassword(e.target.value)}
                        className="input"
                        placeholder="password" required />
                    <div align="center">
                        <button type="submit" className="button">
                            <span>Start Chatting</span>
                        </button>

                    </div>
                    <h2 className="error">{error}</h2>
                </form>
            </div>



        </div>

    );
};


export default LoginForm