import { ChatEngine } from 'react-chat-engine';
import './App.css';
import ChatFeed from './components/ChatFeed';
import LoginForm from './components/LoginForm';

const App =() =>{

	if(!localStorage.getItem('username')) return <LoginForm/>
	return (
		<ChatEngine
		   height="100vh"   
		   projectID='30f4b2e0-e3d6-48bf-bef0-40d82c84dddf'
			userName={localStorage.getItem('username')}
			userSecret={localStorage.getItem('password')}
			renderChatFeed={
				(chatAppProps)=> <ChatFeed { ...chatAppProps}
				/>
			}
		/>
	)
}

export default App;